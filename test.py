import RPi.GPIO as GPIO
from time import sleep
GPIO.setmode(GPIO.BCM)
import sys
sys.path.append("./lib")

from time import *
import smbus
import argparse
import Adafruit_DHT
from w1thermsensor import W1ThermSensor

#defs
mech_r1=7
mech_r2=8
mech_r3=25
mech_relays=[7,8,25]
ss_relays=[17,22]
Address = 0x3f
ADDRESS = Address
leds=[12,16]
_switch= 5
_button=6
sensor = W1ThermSensor()

def setup_mech_relays():
    for r in mech_relays:
        GPIO.setup(r, GPIO.OUT)
        GPIO.output(r, True)
        
def test_mech_relays(t_sleep=1):
    #series of 6 clicks
    for r in mech_relays:
        GPIO.output(r,False)
        sleep(t_sleep)
    for r in mech_relays:
        GPIO.output(r,True)
        sleep(t_sleep)
    
def setup_ss_relays():
    for r in ss_relays:
        GPIO.setup(r, GPIO.OUT)
        GPIO.output(r, False)
        
def test_ss_relays(t_sleep=1):
    #heater / pump
    for r in ss_relays:
        GPIO.output(r,True)
    sleep(t_sleep)
    for r in ss_relays:
        GPIO.output(r,False)
    


def setup_leds():
    for l in leds:
        GPIO.setup(l, GPIO.OUT)
def test_leds(t_sleep=1):
    for l in leds:
        GPIO.output(l,True)
    sleep(t_sleep)
    for l in leds:
        GPIO.output(l,False)
    
def setup_switch():
    GPIO.setup(_switch,GPIO.IN)
def setup_button():
    GPIO.setup(_button,GPIO.IN)
    
def read_button():
    return GPIO.input(_button)
def read_switch():
    return GPIO.input(_switch)
def read_thermo():
    return sensor.get_temperature()

def setup_resources():
    setup_mech_relays()
    setup_ss_relays()
    setup_leds()
    setup_button()
    setup_switch()


def read_dht():
    humidity, temperature = Adafruit_DHT.read_retry(Adafruit_DHT.DHT22, 9)
    temperature = temperature * 9/5.0 + 32
    return 'Temp={0:0.1f}*  Humidity={1:0.1f}%'.format(temperature, humidity)

#LCDDriver

# commands
LCD_CLEARDISPLAY = 0x01
LCD_RETURNHOME = 0x02
LCD_ENTRYMODESET = 0x04
LCD_DISPLAYCONTROL = 0x08
LCD_CURSORSHIFT = 0x10
LCD_FUNCTIONSET = 0x20
LCD_SETCGRAMADDR = 0x40
LCD_SETDDRAMADDR = 0x80

# flags for display entry mode
LCD_ENTRYRIGHT = 0x00
LCD_ENTRYLEFT = 0x02
LCD_ENTRYSHIFTINCREMENT = 0x01
LCD_ENTRYSHIFTDECREMENT = 0x00

# flags for display on/off control
LCD_DISPLAYON = 0x04
LCD_DISPLAYOFF = 0x00
LCD_CURSORON = 0x02
LCD_CURSOROFF = 0x00
LCD_BLINKON = 0x01
LCD_BLINKOFF = 0x00

# flags for display/cursor shift
LCD_DISPLAYMOVE = 0x08
LCD_CURSORMOVE = 0x00
LCD_MOVERIGHT = 0x04
LCD_MOVELEFT = 0x00

# flags for function set
LCD_8BITMODE = 0x10
LCD_4BITMODE = 0x00
LCD_2LINE = 0x08
LCD_1LINE = 0x00
LCD_5x10DOTS = 0x04
LCD_5x8DOTS = 0x00


# flags for backlight control
LCD_BACKLIGHT = 0x08
LCD_NOBACKLIGHT = 0x00

En = 0b00000100 # Enable bit
Rw = 0b00000010 # Read/Write bit
Rs = 0b00000001 # Register select bit
class lcd:
   #initializes objects and lcd
   def __init__(self, ADDRESS=ADDRESS):
      self.lcd_device = i2c_device(ADDRESS)
      self.numlines = 2
      self.lcd_write(0x03)
      self.lcd_write(0x03)
      self.lcd_write(0x03)
      self.lcd_write(0x02) #set 4 bit mode

      self.lcd_write(LCD_FUNCTIONSET | LCD_2LINE | LCD_5x8DOTS | LCD_4BITMODE)
      self.lcd_write(LCD_DISPLAYCONTROL | LCD_DISPLAYON)
      #self.lcd_write(LCD_CLEARDISPLAY)
      self.lcd_write(LCD_ENTRYMODESET | LCD_ENTRYLEFT)
      sleep(0.2)

   # clocks EN to latch command
   def lcd_strobe(self, data):
      self.lcd_device.write_cmd(data | En | LCD_BACKLIGHT)
      sleep(.0005)
      self.lcd_device.write_cmd(((data & ~En) | LCD_BACKLIGHT))
      sleep(.0001)

   def lcd_write_four_bits(self, data):
      self.lcd_device.write_cmd(data | LCD_BACKLIGHT)
      self.lcd_strobe(data)
   def write4bits(self, data):
      self.lcd_device.write_cmd(data | LCD_BACKLIGHT)
      self.lcd_strobe(data)

   # write a command to lcd
   def lcd_write(self, cmd, mode=0):
      self.lcd_write_four_bits(mode | (cmd & 0xF0))
      self.lcd_write_four_bits(mode | ((cmd << 4) & 0xF0))

   #turn on/off the lcd backlight
   def lcd_backlight(self, state):
      if state in ("on","On","ON"):
         self.lcd_device.write_cmd(LCD_BACKLIGHT)
      elif state in ("off","Off","OFF"):
         self.lcd_device.write_cmd(LCD_NOBACKLIGHT)
      else:
         print "Unknown State!"
    # put string function
   def lcd_display_string(self, string, line):
      if line == 1:
         self.lcd_write(0x80)
      if line == 2:
         self.lcd_write(0xC0)
      for char in string:
         self.lcd_write(ord(char), Rs)

   def message(self, string):
      for char in string:
         self.lcd_write(ord(char), Rs)

   # clear lcd and set to home
   def clear(self):
      self.lcd_write(LCD_CLEARDISPLAY)


   def setCursor(self, col, row):
      self.row_offsets = [0x00, 0x40, 0x14, 0x54]
      if row > self.numlines:
         row = self.numlines - 1  # we count rows starting w/0
      self.lcd_write(LCD_SETDDRAMADDR | (col + self.row_offsets[row]))
class i2c_device:
   def __init__(self, addr, port=1):
      self.addr = addr
      self.bus = smbus.SMBus(port)

# Write a single command
   def write_cmd(self, cmd):
      self.bus.write_byte(self.addr, cmd)
      sleep(0.0001)

# Write a command and argument
   def write_cmd_arg(self, cmd, data):
      self.bus.write_byte_data(self.addr, cmd, data)
      sleep(0.0001)

# Write a block of data
   def write_block_data(self, cmd, data):
      self.bus.write_block_data(self.addr, cmd, data)
      sleep(0.0001)

# Read a single byte
   def read(self):
      return self.bus.read_byte(self.addr)

# Read
   def read_data(self, cmd):
      return self.bus.read_byte_data(self.addr, cmd)

# Read a block of data
   def read_block_data(self, cmd):
      return self.bus.read_block_data(self.addr, cmd)


def write_test():
    _lcd = lcd(ADDRESS)
    _lcd.setCursor(0,0)
    _lcd.message("THIS IS A TEST")
    _lcd.setCursor(0,1)
    _lcd.message("SUCH WOW........")
