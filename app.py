from random import randint
from flask import Flask
from flask import render_template
from flask.ext.bootstrap import Bootstrap

from test import *


app = Flask(__name__)
Bootstrap(app)
app.debug=True


from flask_bootstrap import StaticCDN
app.extensions['bootstrap']['cdns']['jquery'] = StaticCDN()
base_page="test.html"

setup_resources()

results={'mech': '',
		'ss':'',
		'leds':'',
		'switch':'',
		'button':'',
		'lcd':'',
		'DHT':'',
		'thm':''
		}



@app.route("/")
def index():
	return "Hello world!"

@app.route('/echo/<something>')
def echos(something):
	return something

@app.route('/test')
def test():
	return render_template("test.html",data=results)
@app.route('/test/<_type>')
def play_test(_type):
	res=''
	if _type == "mech":
		test_mech_relays()
		results.update({'mech':'Ran'})
	if _type == "ss":
		test_ss_relays()
		results.update({'ss':'Ran'})
	if _type == "leds":
		test_leds()
		results.update({'leds':'Ran'})
	if _type == "switch":
		res= read_switch()
		results.update({'switch':res})
	if _type == "button":
		res= read_button()
		results.update({'button':res})
	if _type == 'lcd':
		write_test()
		results.update({'lcd':'Ran'})
	if _type == 'DHT':
		res=read_dht()
		results.update({'DHT':res})
	if _type == 'thm':
		res=read_thermo()
		results.update({'thm':res})

	return render_template("test.html",data=results)






if __name__ == "__main__":
	app.run(host='0.0.0.0', port=5001)


